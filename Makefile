JFLAGS = -g -verbose -d ./*/bin
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

JAVA_FILES:=$(wildcard ./*/src/*/*.java)
JAVA_CLASSES:=$(patsubst %.java,%.class,$(JAVA_FILES))

CLASSES:=$(JAVA_CLASSES)

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM)v ./*/{bin/*,src/*}/*.class
