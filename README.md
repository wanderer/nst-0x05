# nst-0x05

this repo holds *sawce* for project `0x05` of the NST lessons

this im server is currently one big shared room with optional dms (direct messages)

notable functionality
* set/reset username \
    `#set-username <yourusername>`\
    `#reset-username`
* send dms \
    `#dm <peer> <your message here>`
